﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public enum BattleState { IDLE, PLAYERTURN, ENEMYTURN, WON, LOST }
public enum ActionState { ATTACK, SKILL, MAGIC, IDLE }

public class BattleController : MonoBehaviour
{
    public GameObject[] players;
    public GameObject[] enemies;
    public List<Unit> units = new List<Unit>();
    public List<Unit> unitPlayers = new List<Unit>();
    public List<Unit> unitEnemies = new List<Unit>();

    public Transform[] playersPos;
    public Transform[] enemiesPos;

    [SerializeField] private Unit randomPlayer;

    [SerializeField] private Unit unitTurn;

    public int turn;
    public Text turnText;
    public Text logText;
    [SerializeField] private CustomText logCustom;

    public BattleState state;
    public ActionState action;


    // Start is called before the first frame update
    void Start()
    {
        logText.text = "";
        turnText.text = "";
        logCustom = new CustomText();
        state = BattleState.IDLE;
        action = ActionState.IDLE;
        turn = 0;
        for (int i = 0; i < 5; i++)
        {
            unitPlayers.Add(Instantiate(players[i], playersPos[i]).GetComponent<Player>());
        }
        for (int i = 0; i < 5; i++)
        {
            unitEnemies.Add(Instantiate(enemies[i], enemiesPos[i]).GetComponent<Enemy>());
        }
        units.AddRange(unitPlayers);
        units.AddRange(unitEnemies);
        units.Sort();
        StartCoroutine(ChooseTurn());
    }

    IEnumerator ChooseTurn()
    {
        if (state == BattleState.IDLE)
        {

            unitTurn = units[turn % units.Count];
            turn++;
            yield return new WaitForSeconds(0f);

            if (!unitTurn.getLive())
            {
                StartCoroutine(ChooseTurn());
            }
            else
            {
                turnText.text = "Turn " + unitTurn.getName();
                if (unitTurn.CompareTag("Player"))
                {
                    state = BattleState.PLAYERTURN;

                }
                else
                {
                    state = BattleState.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
            }
        }
    }
    
    public void OnAttackClick()
    {
        if (state == BattleState.PLAYERTURN && action == ActionState.IDLE)
        {
            turnText.text = "Choose enemy to attack";
            action = ActionState.ATTACK;
            
        }
        else
        {
            action = ActionState.IDLE;
            turnText.text = "Turn " + unitTurn.getName();
        }
    }

    public IEnumerator ChooseUnit(Unit unitTarget)
    {       
        if (action == ActionState.ATTACK && unitTarget.CompareTag("Enemy") && unitTarget.getLive())
        {
            action = ActionState.IDLE;
            state = BattleState.IDLE;
            logCustom.AddText(string.Format("{0} attacks {1}\n", unitTurn.getName(), unitTarget.getName()));
            logText.text = logCustom.TextToString();
            float damage = unitTurn.getDamage();
            unitTarget.TakeDamage(damage);
            if (unitTarget.getHealth() == 0)
            {
                unitEnemies.Remove(unitTarget);
            }
            logCustom.AddText(string.Format("{0} received {1} damage\n", unitTarget.getName(), damage));
            logText.text = logCustom.TextToString();
            yield return new WaitForSeconds(0f);
            StartCoroutine(ChooseTurn());
           
        }
        
    }

    IEnumerator EnemyTurn()
    {

        randomPlayer = unitPlayers[Random.Range(0, unitPlayers.Count - 1)];
        randomPlayer.TakeDamage(unitTurn.getDamage());
        if (randomPlayer.getHealth() == 0)
        {
            unitPlayers.Remove(randomPlayer);
        }
        yield return new WaitForSeconds(0f);
        state = BattleState.IDLE;
        StartCoroutine(ChooseTurn());
    }




    
}
