﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CustomText
{ 

    private List<string> listText;

    public CustomText()
    {
        listText = new List<string>();
    }

    public void AddText(string text)
    {
        listText.Add(text);
        if (listText.Count > 10)
        {
            listText.RemoveAt(0);
        }
    }

    public List<string> getText()
    {
        return listText;
    }

    public string TextToString()
    {
        string temp = "";
        foreach (string text in listText)
        {
            temp += text;
        }
        return temp;
    }

}
