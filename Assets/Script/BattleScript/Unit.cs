﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Unit : MonoBehaviour, IComparable<Unit>
{
    [SerializeField] private string name;
    [SerializeField] private float maxHealth;
    [SerializeField] private float health;
    [SerializeField] private float speed;
    [SerializeField] private float damage;
    [SerializeField] private bool isLive;

    public SpriteRenderer spriteObject;
    

    [SerializeField] private GameObject system;



    public HealthBar healthBar;

    private void Start()
    {
        system = GameObject.Find("BattleSystem");
        healthBar.setMaxHealth(maxHealth);
        healthBar.setHealth(health);
        isLive = true;
    }

    public bool getLive()
    {
        return isLive;
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            health = 0;
            isLive = false;
            spriteObject.enabled = false;
            Destroy(healthBar.gameObject);
        }
        healthBar.setHealth(health);
    }

    public float getHealth()
    {
        return health;
    }

    public float getSpeed()
    {
        return speed;
    }

    public float getDamage()
    {
        return damage;
    }

    public string getName()
    {
        return name;
    }

    
    public int CompareTo(Unit other)
    {
        if (other == null)
        {
            return 1;
        }

        return Mathf.RoundToInt(other.getSpeed() - this.getSpeed());

    }

    public void OnChooseUnit()
    {
        BattleController var = system.GetComponent<BattleController>();
        StartCoroutine(var.ChooseUnit(this));
    }
}
